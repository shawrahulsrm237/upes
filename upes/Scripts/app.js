// fixed navigation



$(function() {
	//----- OPEN
	$('[data-popup-open]').on('click', function(e)  {
		var targeted_popup_class = jQuery(this).attr('data-popup-open');
		$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		e.preventDefault();
	});

	//----- CLOSE
	$('[data-popup-close]').on('click', function(e)  {
		var targeted_popup_class = jQuery(this).attr('data-popup-close');
		$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);

		e.preventDefault();
	});
});



jQuery("document").ready(function($){

	var nav = $('header');

	$(window).scroll(function () {
		if ($(this).scrollTop() > 150) {
			nav.addClass("f-nav");
		} else {
			nav.removeClass("f-nav");
		}
	});

});

// sub menu fixed

$(function(){
        // Check the initial Poistion of the Sticky Header
        var stickyHeaderTop = $('#stickyheader').offset().top;
		if( $(window).width() > 768)
		{
			$(window).scroll(function(){
					if( $(window).scrollTop() > stickyHeaderTop ) {
							$('#stickyheader').css({position: 'fixed', top: '61px'});
					} else {
							$('#stickyheader').css({position: 'static', top: '0px'});
					}
			});	
		}else{
			
	
			$('#stickyheader').css({position: 'static', top: 'initial'});
			
			
		}	

  });


	$(function(){
	        // Check the initial Poistion of the Sticky Header
	        var stickyHeaderTop = $('#sticky_div').offset().top;

	        $(window).scroll(function(){
	                if( $(window).scrollTop() > stickyHeaderTop ) {
	                        $('#sticky_div').css({position: 'fixed', top: '120px'});
													$('#sticky_div').addClass('sticky_div');
													$( '#sticky_div' ).children('.mk').addClass('container');
	                } else {
	                        $('#sticky_div').css({position: 'static', top: '0px'});
													$('#sticky_div').removeClass('sticky_div');
													$( '#sticky_div' ).children('.mk').removeClass('container');
	                }
	        });
	  });




// home page carousel
$('.home-banner .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    nav:false,
    dots:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
// anual report carousel


var owl = $(".annual-report .owl-carousel");
owl.owlCarousel({
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

// jQuery method on
owl.on('changed.owl.carousel',function(property){
    var current = property.item.index;
    var src = $(property.target).find(".owl-item").eq(current).find("img").attr('data-text');
	var strArray = src.split(",");
	$('.download-report h2').text(strArray[0]);
	$('.download-report a').attr("href",strArray[1]);
    console.log(strArray[0]);
	
});


// award section carousel
$('.award-accolades .owl-carousel').owlCarousel({
    loop:true,
    margin:15,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
})
// regular recruiters section carousel
$('.regular-recruiters .owl-carousel').owlCarousel({
    loop:true,
    margin:15,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    nav:false,
    dots:false,
    responsive:{
        0:{
            items:3
        },
        600:{
            items:4
        },
        1000:{
            items:6
        }
    }
})

$('.acdmy_partner_scroll .owl-carousel').owlCarousel({
    loop:true,
    margin:15,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    nav:true,
    dots:true,
    responsive:{
        0:{
            items:3
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
})

$('.acdmy_facilities_scroll .owl-carousel').owlCarousel({
    loop:true,
    margin:15,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    nav:true,
    dots:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

// News Events section carousel
$('.news-event .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
// GALLERY section carousel
$('.gallery .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    dots:false,
    responsive:{
        0:{
            items:2,
            autoWidth:false,
        },
        768:{
            autoWidth:true,
            items:3
        },
        1000:{
            autoWidth:true,
            items:4
        }
    }
})
// Who we are page carousel
$('.program-offered .owl-carousel').owlCarousel({
    loop:true,
    margin:15,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    dots:false,
		nav:true,
    responsive:{
        0:{
            items:2
        },
        768:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
// key facts carousel
$('.key-facts .owl-carousel').owlCarousel({
    loop:true,
    margin:48,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    dots:false,
		nav:true,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
// testimonials carousel
$('.testimonials .owl-carousel').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:2500,
    autoplayHoverPause:true,
    dots:false,
		nav:true,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

//slider for panel below the head div
$("#msubmneu").click(function(){
  $("#smenu").slideToggle("slow");
  $(this).toggleClass('active');
});

// mobile mmenu

$(function(){

	$('.mobile_trigger').click(function(){
		$('.mobile_menu, .mobile_bg').animate({'left':0});
		$('.mobile_hover').fadeIn();
	});
	$('.mobile_hover').click(function(){
		$('.mobile_menu, .mobile_bg').animate({'left':-280});
		$('.mobile_hover').fadeOut();

	});
	$('.multi').click(function(){
		$(this).toggleClass('active1');
    $(this).next('ul').slideToggle();
	});
	$('.smulti').click(function(){
		$(this).toggleClass('active');
    $(this).next('div').slideToggle();
	});
	$('.fmulti').click(function(){
		$(this).toggleClass('active');
		//console.log($(this).parent().next('ul').html());
    $(this).parent().next('ul').slideToggle();
	});


});

$(document).ready(function(){
  $(".gallery-popup .owl-carousel").owlCarousel({

    items: 1,
		stagePadding: 250,
		loop:true

  });
});


// the following to the end is whats needed for the thumbnails.
jQuery( document ).ready(function() {


        // 1) ASSIGN EACH 'DOT' A NUMBER
			dotcount = 1;

			jQuery('.gallery-popup  .owl-dot').each(function() {
			  jQuery( this ).addClass( 'dotnumber' + dotcount);
			  jQuery( this ).attr('data-info', dotcount);
			  dotcount=dotcount+1;
			});

			 // 2) ASSIGN EACH 'SLIDE' A NUMBER
			slidecount = 1;

			jQuery('.gallery-popup .owl-item').not('.cloned').each(function() {
			  jQuery( this ).addClass( 'slidenumber' + slidecount);
			  slidecount=slidecount+1;
			});

			// SYNC THE SLIDE NUMBER IMG TO ITS DOT COUNTERPART (E.G SLIDE 1 IMG TO DOT 1 BACKGROUND-IMAGE)
			jQuery('.gallery-popup .owl-dot').each(function() {

          grab = jQuery(this).data('info');

          slidegrab = jQuery('.slidenumber'+ grab +' img').attr('src');
          console.log(slidegrab);

          jQuery(this).css("background-image", "url("+slidegrab+")");

      });

			// THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
        // TO MAKE IT ALL NEAT
			amount = jQuery('.gallery-popup .owl-dot').length;
			gotowidth = 100/amount;

			jQuery('.gallery-popup .owl-dot').css("width", gotowidth+"%");
			newwidth = jQuery('.owl-dot').width();
			jQuery('.gallery-popup .owl-dot').css("height", newwidth+"px");



});
/* Courese master page smooth scroll */

(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 200)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sticky_div',
    offset: 200
  });

})(jQuery); // End of use strict
