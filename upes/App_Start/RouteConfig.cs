﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace upes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Page",
                url: "Page/Index/{id}",
                defaults: new { controller = "Page", action = "Index",id=UrlParameter.Optional}
            );
            routes.MapRoute(
                name: "FacultyProfile",
                url: "Faculty/Index/{id}",
                defaults: new { controller = "Faculty", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "DepartmentFaculty",
                url: "Schools/DepartmentFaculty/{deptId}",
                defaults: new { controller = "Schools", action = "DepartmentFaculty", deptId = UrlParameter.Optional }
            );
            routes.MapRoute(
              name: "ProgrammesCourseMaster",
              url: "Programmes/CourseMaster/{id}",
              defaults: new { controller = "Programmes", action = "CourseMaster" }
            );
        }
    }
}
