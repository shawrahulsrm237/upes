﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Data;

namespace upes.Models
{
    public class TestingNight
    {
        public int Pageid { get; set; }
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }
        public String SmallDescription { get; set; }
        public String contentid { get; set; }

        public String courseId { get; set; }
        public String detail { get; set; }
        public String courseName { get; set; }
        public String courseDetail { get; set; }


        private SqlConnection connection;
        public DataSet getSchoolMaster()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Select schid, schoolname From School_Master where Status=1 order by displayorder", connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet getCourseLevel()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Select levelid, levelname From CourseLevel_Master where Status=1 order by displayorder", connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet getUnderGraduateCourses()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "select Course.courseid,Course.coursename from Course join College_course on Course.courseid=College_course.courseid where Course.levelid=1";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet getPostGraduateCourses()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "select Course.courseid,Course.coursename from Course join College_course on Course.courseid=College_course.courseid where Course.levelid=2";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet getCourses(String levelId,String schoolId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "select Course.courseid,Course.coursename from Course join College_course on Course.courseid=College_course.courseid where Course.levelid=1 and College_course.schid=1";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public DataSet getCourseEligibility(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "select contentid,courseid,detail from Course_Eligibility where courseid=10";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public void getCourseCurriculum(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("select contentid,courseid,detail from Course_curriculum where courseid=20");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    contentid = reader.GetInt32(0).ToString();
                    courseId = reader.GetInt32(0).ToString();
                    detail = reader.GetString(2);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }
        }
        public void getCourseCareerProspects(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("select contentid,courseid,detail from Course_Prospects where courseid=20");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    //contentid = reader.GetString(0);
                    contentid = reader.GetInt32(0).ToString();
                    detail = reader.GetString(2);                    
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }
        }
        public void getCourseFeeStructure(String courseId)
        {
            //connection = new SqlConnection();
            //connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            //String qry = "select contentid,courseid,detail from Course_fee where courseid=20";
            //SqlCommand cmd = new SqlCommand(qry, connection);
            //DataSet ds = new DataSet();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(ds);
            //return ds;

            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("select contentid,courseid,detail from Course_fee where courseid=20");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    contentid = reader.GetInt32(0).ToString();
                    courseId = reader.GetInt32(1).ToString();
                    detail = reader.GetString(2);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }
        }

        public DataSet getCourseFaculty(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "select facultyid,fname,Designation from Addfacultymaster";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public DataSet getPlacementRecords(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "select contentid,courseid,detail from Course_Placement where courseid=10";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }

        public void getAdmissionCriteria(String courseId)
        {
            //connection = new SqlConnection();
            //connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            //String qry = "select contentid,courseid,detail,duration from Course_Eligibility where courseid=10";
            //SqlCommand cmd = new SqlCommand(qry, connection);
            //DataSet ds = new DataSet();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(ds);
            //return ds;

            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("select contentid,courseid,detail from Course_admission where courseid=20");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    //contentid = reader.GetString(0);
                    contentid = reader.GetInt32(0).ToString();
                    detail = reader.GetString(2);                    
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }
        }

        public void getCourseMaster()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription,smalldesc FROM PageMaster where Pageid=314");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                    SmallDescription = reader.GetString(6);
                }


            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getCourseIntro(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("select courseid,coursename,coursedetail from Course where courseid=20");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    courseId = reader.GetInt32(0).ToString();
                    courseName = reader.GetString(1);
                    courseDetail = reader.GetString(2);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }
        }

        public void getCourseTestimonials(String courseId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("select courseid,coursename,coursedetail from Course where courseid=20");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    courseId = reader.GetInt32(0).ToString();
                    courseName = reader.GetString(1);
                    courseDetail = reader.GetString(2);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }
        }

    }
}