﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Data;

namespace upes.Models
{
    public class PressReleaseModel
    {
        private SqlConnection connection;

        public int Pageid { get; set; }
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }
        public String SmallDescription { get; set; }
        public DataSet getPressRelease()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Select Pressid,Ptitle,shortDesc,PDesc,Uploadfile From Pressrelease where Status=1 order by displayorder", connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
    }
}