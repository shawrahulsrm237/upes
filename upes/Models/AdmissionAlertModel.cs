﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Data;

namespace upes.Models
{
    public class AdmissionAlertModel
    {
        private SqlConnection connection;
        public DataSet getAdmissionAlerts()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Select alertid, alertname, displayorder From alert_type where Status=1 order by displayorder", connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        
         public DataSet getAlerts(String alertId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            String qry = "Select aid,description,alertid,descnew From alerts where Status=1 and alertid="+alertId+"order by displayorder";
            SqlCommand cmd = new SqlCommand(qry, connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
    }
}