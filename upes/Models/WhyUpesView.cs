﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
namespace upes.Models
{
    public class WhyUpesView
    {
        private SqlConnection connection;
        public int Pageid { get; set; }
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }

        public void getWhyUpesIndexData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=169");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesAdvantageUpesData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=32");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesLifeAtUpesData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=33");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesPlacementRecordsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=37");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesTeachingMethodologyData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=34");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesCsrData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=229");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesConferencesData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=43");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesFunNeverEndsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=321");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesMeetTheExpertsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=322");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesEnjoyYourSpaceData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=323");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getWhyUpesMakeAContributionData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=324");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }
    }
}