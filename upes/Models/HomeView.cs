﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Data;

namespace upes.Models
{
    public class HomeView
    {
        private SqlConnection connection;
        public string name { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string message { get; set; }
        public int Pageid { get; set; }
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }
        
        public void getHomeIndexData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=1");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void submitFormData(String name, String Mobile, String email, String message)
        {
            String countryname = "Rahul Shaw";
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = connection;
            cmd.CommandText = string.Format("insert into country(Countrycode,countryname,status,trdate) values('10101','1111','TRUE','2013-09-19 16:34:38.703')");
            connection.Open();
            //cmd.Parameters.AddWithValue("@countryname", countryname);
            //cmd.Parameters.AddWithValue("@mobile", mobile);
            //cmd.Parameters.AddWithValue("@email", email);
            //cmd.Parameters.AddWithValue("@message", message);
            cmd.CommandType = CommandType.Text;
            int i = cmd.ExecuteNonQuery();
            connection.Close();
            
        }


    }
}