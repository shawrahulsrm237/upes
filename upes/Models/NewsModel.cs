﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Data;
namespace upes.Models
{
    public class NewsModel
    {
        private SqlConnection connection;
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }
        public String SmallDescription { get; set; }

        public int NewsId { get; set; }
        public String NewsDesc{ get; set; }
        public String NewsDate { get; set; }
        public String NewsTitle { get; set; }

        public DataSet getNews()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand("select NewsId,NewsTitle, NewsDate from News where status =1 order by NewsId", connection);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            return ds;
        }
        public void getNewsDetails(String id)
        {
            String qry = "select NewsId,NewsTitle, NewsDate,NewsDesc from News where status=1 and NewsId="+id;
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format(qry);
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    NewsTitle = reader.GetString(1);
                    NewsDate = reader.GetDateTime(2).ToString();
                    NewsDesc = reader.GetString(3);

                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }

        }
    }
}