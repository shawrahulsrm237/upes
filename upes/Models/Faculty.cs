﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;

namespace upes.Models
{
    public class Faculty

    {
        private SqlConnection connection;

        
        public int FacultyId { get; set; }
        public String Fname { get; set; }
        public String Designation { get; set; }
        public String FImage { get; set; }

        public String SmallDesc { get; set; }
        public String DetailDesc { get; set; }

        public String Achivement { get; set; }
        

        public void getFacultyProfile(String facultyId)
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            String qry = "SELECT facultyid,fname,Designation,fimage,smalldesc,detaildesc,achivement FROM Addfacultymaster where status=1 and facultyid =" + facultyId;
            cmd.CommandText = string.Format(qry);
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    FacultyId = reader.GetInt32(0);
                    Fname = reader.GetString(1);
                    Designation = reader.GetString(2);
                    FImage = reader.GetString(3);
                    SmallDesc = reader.GetString(4);
                    DetailDesc = reader.GetString(5);
                    Achivement = reader.GetString(6);

                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }


    }
}