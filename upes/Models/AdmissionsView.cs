﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;

namespace upes.Models
{
    public class AdmissionsView
    {
        private SqlConnection connection;

        


        public int AboutId { get; set; }
        public String AboutDescription { get; set; }

        public int Pageid { get; set; }
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }

        public String SmallDescription { get; set; }

        public void getAdmissionsIndexData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=222");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsAdmissionData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=311");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsScholarshipsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=195");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsFeeRefundPolicyData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=293");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsFinancialAssistanceData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=53");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsInternationalStudentsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=54");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsOnlineApplicationFormData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=55");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsApplicantLoginData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=311");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionsEntranceExamData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=56");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }


        public void getAdmissionsFaqsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=57");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionAlertData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription,smalldesc FROM PageMaster where Pageid=208");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                    SmallDescription = reader.GetString(6);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getAdmissionCriteriaData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=52");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }


    }
}