﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;


namespace upes.Models
{
    public class PlacementView
    {
        private SqlConnection connection;

        public int AboutId { get; set; }
        public String AboutDescription { get; set; }

        public int Pageid { get; set; }
        public String PageName { get; set; }
        public String PageTitle { get; set; }
        public String PageMeta { get; set; }
        public String PageMetaDesc { get; set; }
        public String PageDescription { get; set; }

        public void getPlacementIndexData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=74");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getPlacementPlacementsRecordsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=76");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getPlacementSummerInternshipsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=77");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getPlacementOfficeCareerServicesData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=79");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getPlacementOurPatronsData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=75");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getPlacementPsiProgramData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=80");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }

        public void getPlacementCampusHiringInterestFormData()
        {
            connection = new SqlConnection();
            connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLDbConnection"].ConnectionString;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            cmd.CommandText = string.Format("SELECT Pageid,PageName,PageTitle,PageMeta,PageMetaDesc,PageDescription FROM PageMaster where Pageid=84");
            connection.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            try
            {
                reader.Read();
                if (reader.IsDBNull(0) == false)
                {
                    Pageid = reader.GetInt32(0);
                    PageName = reader.GetString(1);
                    PageTitle = reader.GetString(2);
                    PageMeta = reader.GetString(3);
                    PageMetaDesc = reader.GetString(4);
                    PageDescription = reader.GetString(5);
                }

            }
            catch (SqlException e)
            {
                string MessageString = "Read error occurred  / entry not found loading the Column details: "
                + e.ErrorCode + " - " + e.Message + "; \n\nPlease Continue";
                reader.Close();
            }


        }
    }
}