﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class ProgrammesController : Controller
    {
        // GET: Programmes
        public ActionResult Index()
        {
            ProgrammesView pv = new ProgrammesView();
            pv.getProgrammesIndexData();
            ViewBag.Title = pv.PageTitle;
            ViewBag.Description = pv.PageMetaDesc;
            return View(pv);
        }
        public ActionResult SchoolWise()
        {
            ProgrammesView pv = new ProgrammesView();
            pv.getProgrammesSchoolWiseData();
            ViewBag.Title = pv.PageTitle;
            ViewBag.Description = pv.PageMetaDesc;
            return View(pv);
        }

        [HttpGet]
        public ActionResult CoursePrograms()
        {
            CoursesModel cm = new CoursesModel();
            DataSet dsCoursePrograms = cm.getCoursePrograms();
            return View(dsCoursePrograms);
        }

        [HttpGet]
        public ActionResult CourseLevel()
        {
            CoursesModel cm = new CoursesModel();
            DataSet dsCourseLevel = cm.getCourseLevel();
            return View(dsCourseLevel);
        }

        [HttpGet]
        public ActionResult Course(String id)
        {
            CoursesModel cm = new CoursesModel();
            DataSet dsCourse = cm.getCourse(id);
            return View(dsCourse);

        }
        
        [HttpGet]
        public ActionResult CourseMaster(String id)
        {
            ProgrammesView pv = new ProgrammesView();
            pv.getProgrammesCourseMasterData();
            ViewBag.Title = pv.PageTitle;
            ViewBag.Description = pv.PageMetaDesc;
            return View(pv);
        }
    }
}