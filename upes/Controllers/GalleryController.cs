﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class GalleryController : Controller
    {
        // GET: Gallery
        public ActionResult Index()
        {
            GalleryView av = new GalleryView();
            av.getGalleryIndexData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;

            DataSet dsAlbum = av.getAlbums();
            DataSet dsAlbumPhoto = av.getAlbumsPhoto(2);
            ViewBag.Albums = dsAlbum.Tables[0];
            ViewBag.AlbumsPhoto = dsAlbumPhoto.Tables[0];

            return View(av);
        }
        
        public ActionResult Index2()
        {

            GalleryView av = new GalleryView();
            DownloadModels d = new DownloadModels();
            DataSet dsDownloads = d.getDownloads();
            ViewBag.Downloads = dsDownloads.Tables[0];

            return View();
        }
    }
}