﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class NewsController : Controller
    {
        // GET: News
        public ActionResult Index()
        {
            NewsModel nm = new NewsModel();
            DataSet dsNews = nm.getNews();
            ViewBag.News = dsNews.Tables[0];
            return View(nm);
            
        }
        public ActionResult Detail(String id)
        {
            NewsModel nm = new NewsModel();
            nm.getNewsDetails(id);
            ViewBag.NewsTitle = nm.NewsTitle;
            ViewBag.NewsDate = nm.NewsDate;
            ViewBag.NewsDesc = nm.NewsDesc;
            return View(nm);            
        }
    }
}