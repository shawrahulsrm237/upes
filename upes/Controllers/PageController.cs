﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class PageController : Controller
    {
        // GET: Page
        public ActionResult Index(String id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult Show(String id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpGet]
        public ActionResult LoadPage(String id)
        {
            PageModel pm = new PageModel();
            pm.getPageData(id);
            ViewBag.Pageid = pm.Pageid;
            ViewBag.PageName = pm.PageName;
            ViewBag.PageTitle = pm.PageTitle;
            ViewData["PageTitle"] = pm.PageTitle;
            return View(pm);
        }
    }
}