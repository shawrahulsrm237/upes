﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class SchoolsController : Controller
    {
        // GET: Schools
        public ActionResult Index()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsIndexData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }
        public ActionResult OurFaculty()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsOurFacultyData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            DataSet dsDepartment = sv.getDepartments();
            ViewBag.Departments = dsDepartment.Tables[0];
            return View(sv);
        }
        public ActionResult OurFaculty1()
        {
            SchoolsView sv = new SchoolsView();
            DataSet dsDepartment = sv.getDepartments();
            ViewBag.Departments = dsDepartment.Tables[0];
            return View (sv);
        }
        [HttpGet]
        public ActionResult DepartmentFaculty(String deptId)
        {
            SchoolsView aam = new SchoolsView();
            DataSet dsDepartmentFaculty = aam.getDepartmentFaculty(deptId);
            return View(dsDepartmentFaculty);
        }
        public ActionResult SchoolOfEngineering()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsSchoolOfEngineeringData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }
        public ActionResult SchoolOfBussiness()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsSchoolOfBussinessData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }

        public ActionResult SchoolOfLaw()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsSchoolOfLawData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }

        public ActionResult SchoolOfDesign()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsSchoolOfDesignData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }

        public ActionResult SchoolOfComputerScience()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsSchoolOfComputerScienceData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }



        public ActionResult WorldClassFaculty()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsWorldClassFacultyData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }

        public ActionResult VisitingFaculty()
        {
            SchoolsView sv = new SchoolsView();
            sv.getSchoolsVisitingFacultyData();
            ViewBag.Title = sv.PageTitle;
            ViewBag.Description = sv.PageMetaDesc;
            return View(sv);
        }


    }
}