﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class WhyupesController : Controller
    {
        // GET: Whyupes
        public ActionResult Index()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesIndexData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
        public ActionResult AdvantageUpes()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesAdvantageUpesData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
        public ActionResult LifeAtUpes()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesLifeAtUpesData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
        public ActionResult PlacementRecords()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesPlacementRecordsData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
        public ActionResult TeachingMethodology()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesTeachingMethodologyData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
        public ActionResult Csr()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesCsrData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
        public ActionResult Conferences()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesConferencesData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }

        public ActionResult FunNeverEnds()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesFunNeverEndsData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }

        public ActionResult MeetTheExperts()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesMeetTheExpertsData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }

        public ActionResult EnjoyYourSpace()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesEnjoyYourSpaceData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }

        public ActionResult MakeAContribution()
        {
            WhyUpesView wu = new WhyUpesView();
            wu.getWhyUpesMakeAContributionData();
            ViewBag.Title = wu.PageTitle;
            ViewBag.Description = wu.PageMetaDesc;
            return View(wu);
        }
    }
}