﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class ProgrammeController : Controller
    {
        // GET: TestingNight
        public ActionResult Index()
        {
            ProgrammesView pv = new ProgrammesView();
            pv.getProgrammesIndexData();
            ViewBag.Title = pv.PageTitle;
            ViewBag.Description = pv.PageMetaDesc;
            return View(pv);
        }
        public ActionResult UnderGraduatePrograms()
        {
            return View();
        }
        public ActionResult PostGraduatePrograms()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UnderGraduateCourses()
        {
            TestingNight aam = new TestingNight();
            DataSet dsCourses = aam.getUnderGraduateCourses();
            return View(dsCourses);
        }
        [HttpGet]
        public ActionResult PostGraduateCourses()
        {
            TestingNight aam = new TestingNight();
            DataSet dsCourses = aam.getPostGraduateCourses();
            return View(dsCourses);
        }

        
        public ActionResult SchoolWise()
        {
            TestingNight tn = new TestingNight();
            DataSet dsSchoolMaster = tn.getSchoolMaster();
            ViewBag.SchoolMaster = dsSchoolMaster.Tables[0];
            return View();
        }

        [HttpGet]
        public ActionResult LoadNights(String id)
        {
            TestingNight aam = new TestingNight();
            DataSet dsAlerts = aam.getCourseLevel();
            return View(dsAlerts);

        }

        [HttpGet]
        public ActionResult LoadStars(String levelId)
        {
            TestingNight aam = new TestingNight();
            DataSet dsCourses = aam.getCourses("1", levelId);
            return View(dsCourses);
        }

        public ActionResult CourseMaster()
        {
            TestingNight pv = new TestingNight();
            pv.getCourseMaster();
            ViewBag.Title = pv.PageTitle;
            ViewBag.Description = pv.PageMetaDesc;
            return View(pv);
        }

        [HttpGet]
        public ActionResult CareerProspects(String id)
        {
            TestingNight pv = new TestingNight();
            pv.getCourseCareerProspects(id);
            ViewBag.detail = pv.detail;
            return View(pv);
            
        }
        [HttpGet]
        public ActionResult CareerCurriculum(String id)
        {
            TestingNight pv = new TestingNight();
            pv.getCourseCurriculum(id);
            //ViewBag.coursed = pv.courseId;
            //ViewBag.courseId = pv.courseId;
            ViewBag.detail = pv.detail;
            return View(pv);

        }

        [HttpGet]
        public ActionResult CourseIntro(String id)
        {
            TestingNight pv = new TestingNight();
            pv.getCourseIntro(id);
            ViewBag.courseId = pv.courseId;
            ViewBag.courseName = pv.courseName;
            ViewBag.courseDetail = pv.courseName;            
            return View(pv);

        }

        [HttpGet]
        public ActionResult AdmissionCriteria(String id)
        {
            TestingNight pv = new TestingNight();
            pv.getAdmissionCriteria(id);
            ViewBag.detail = pv.detail;
            return View(pv);

        }

        [HttpGet]
        public ActionResult CourseFeeStructure(String id)
        {
            TestingNight pv = new TestingNight();
            pv.getCourseFeeStructure(id);
            ViewBag.detail = pv.detail;
            return View(pv);

        }
        [HttpGet]
        public ActionResult CourseTestimonials(String id)
        {
            TestingNight pv = new TestingNight();
            pv.getCourseTestimonials(id);
            ViewBag.detail = pv.detail;
            return View(pv);

        }
    }
}