﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;


namespace upes.Controllers
{
    public class AboutController : Controller
    {
        // GET: About
        public ActionResult Index()
        {
            AboutView av = new AboutView();
            av.getAboutIndexData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            
            return View(av);
        }
        
        public ActionResult WhoWeAre()
        {
            AboutView av = new AboutView();
            av.getAboutWhoWeAreData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult Leadership()
        {
            AboutView av = new AboutView();
            av.getAboutLeadershipData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult InternationalOperations()
        {
            AboutView av = new AboutView();
            av.getAboutInternationalOperationsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult IndustryAlliances()
        {
            AboutView av = new AboutView();
            av.getAboutIndustryAlliancesData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }

        public ActionResult Messages()
        {
            AboutView av = new AboutView();
            av.getAboutMessagesData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult AccreditationsRecognitions()
        {
            AboutView av = new AboutView();
            av.getAboutAccreditationsRecognitionsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);            
        }
        public ActionResult OurVision()
        {
            AboutView av = new AboutView();
            av.getAboutOurVisionData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult Collaborations()
        {
            AboutView av = new AboutView();
            av.getAboutCollaborationsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult Downloads()
        {
            DownloadModels d = new DownloadModels();
            DataSet dsDownloads = d.getDownloads();
            ViewBag.Downloads = dsDownloads.Tables[0];

            return View();
        }
    }
}