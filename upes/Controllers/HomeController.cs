﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            HomeView av = new HomeView();
            av.getHomeIndexData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        [HttpPost]
        public ActionResult HomeAskQuestion(HomeView sm)
        {
            string value = "name: " + Convert.ToString(sm.name)
                + "<br />Mobile: " + sm.mobile
                + "<br />Email: " + sm.email
                + "<br />Message: " + Convert.ToString(sm.message);

            string s = "$('#output').html('" + value + "');";
            HomeView av = new HomeView();
            av.submitFormData(sm.name, sm.mobile, sm.email, sm.message);
            ViewBag.success = "Success";
            return RedirectToAction("Index");            
        }
    }
    
    
}
