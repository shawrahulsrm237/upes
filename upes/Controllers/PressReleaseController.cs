﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class PressReleaseController : Controller
    {
        // GET: PressRelease
        public ActionResult Index()
        {
            PressReleaseModel d = new PressReleaseModel();
            DataSet dsPressRelease = d.getPressRelease();
            ViewBag.PressRelease = dsPressRelease.Tables[0];
            return View();
        }
    }
}