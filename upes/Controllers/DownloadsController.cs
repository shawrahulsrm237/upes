﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class DownloadsController : Controller
    {
        // GET: Downloads
        public ActionResult Index()
        {
            DownloadModels d = new DownloadModels();
            DataSet dsDownloads = d.getDownloads();
            ViewBag.Downloads = dsDownloads.Tables[0];
            return View();
        }
    }
}