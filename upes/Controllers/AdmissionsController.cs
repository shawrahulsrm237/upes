﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class AdmissionsController : Controller
    {
        // GET: Admissions
        public ActionResult Index()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsIndexData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult Admission()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsAdmissionData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult Scholarships()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsScholarshipsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult FeeRefundPolicy()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsFeeRefundPolicyData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult FinancialAssistance()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsFinancialAssistanceData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult InternationalStudents()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsInternationalStudentsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult OnlineApplicationForm()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsOnlineApplicationFormData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult ApplicantLogin()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsApplicantLoginData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult EntranceExam()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsEntranceExamData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult Faqs()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionsFaqsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }

        public ActionResult AdmissionAlert()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionAlertData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;

            AdmissionAlertModel aam = new AdmissionAlertModel();
            DataSet dsAdmissionAlerts = aam.getAdmissionAlerts();
            ViewBag.AdmissionAlerts = dsAdmissionAlerts.Tables[0];
            return View(av);
        }

        [HttpGet]
        public ActionResult LoadAlerts(String id)
        {
            AdmissionAlertModel aam = new AdmissionAlertModel();
            DataSet dsAlerts = aam.getAlerts(id);
            //ViewBag.Alerts = dsAlerts.Tables[0];

            //return ViewBag;
            return View(dsAlerts);
        }


        public ActionResult Criteria()
        {
            AdmissionsView av = new AdmissionsView();
            av.getAdmissionCriteriaData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }

    }
}