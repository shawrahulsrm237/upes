﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class StudentAchievementsController : Controller
    {
        // GET: StudentAchievements
        public ActionResult Index()
        {
            StudentAchievements av = new StudentAchievements();
            av.getIndexData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
    }
}