﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class PlacementController : Controller
    {
        // GET: Placement
        public ActionResult Index()
        {
            PlacementView av = new PlacementView();
            av.getPlacementIndexData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);            
        }
        public ActionResult PlacementsRecords()
        {
            PlacementView av = new PlacementView();
            av.getPlacementPlacementsRecordsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult SummerInternships()
        {
            PlacementView av = new PlacementView();
            av.getPlacementSummerInternshipsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult OfficeCareerServices()
        {
            PlacementView av = new PlacementView();
            av.getPlacementOfficeCareerServicesData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult OurPatrons()
        {
            PlacementView av = new PlacementView();
            av.getPlacementOurPatronsData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        public ActionResult PsiProgram()
        {
            PlacementView av = new PlacementView();
            av.getPlacementPsiProgramData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
        
        public ActionResult CampusHiringInterestForm()
        {
            PlacementView av = new PlacementView();
            av.getPlacementCampusHiringInterestFormData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;
            return View(av);
        }
    }
}