﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class FacultyController : Controller
    {
        // GET: Faculty
        public ActionResult Index(String id)
        {
            Faculty av = new Faculty();
            av.getFacultyProfile(id);
            ViewBag.FacultyId = av.FacultyId;
            ViewBag.Fname = av.Fname;
            ViewBag.Designation = av.Designation;
            ViewBag.FImage = av.FImage;
            ViewBag.SmallDesc = av.SmallDesc;
            ViewBag.DetailDesc = av.DetailDesc;
            ViewBag.Achivement = av.Achivement;
            return View(av);
        }
    }
}