﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;


namespace upes.Controllers
{
    public class IqacController : Controller
    {
        // GET: Iqac
        public ActionResult Index()
        {
            Iqac av = new Iqac();
            av.getIqacData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;

            return View(av);
        }
    }
}