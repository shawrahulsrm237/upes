﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;

namespace upes.Controllers
{
    public class NirfRankingController : Controller
    {
        // GET: NirfRanking
        public ActionResult Index()
        {
            NirfRanking av = new NirfRanking();
            av.getNirfRankingData();
            ViewBag.Title = av.PageTitle;
            ViewBag.Description = av.PageMetaDesc;

            return View(av);
        }
    }
}