﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using upes.Models;


namespace upes.Controllers
{
    public class AwardsController : Controller
    {
        // GET: Awards
        public ActionResult Index()
        {
            AwardsModel am = new AwardsModel();
            DataSet dsAwards = am.getAwards();
            ViewBag.News = dsAwards.Tables[0];
            return View(am);
        }
    }
}